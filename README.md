# Basic blog

## Commands

* Start the app : `npm start`
* Run the dev env : `npm run dev`

## Troubleshooting

* If the app doesn't start properly make sure to add a fake schema.json (this concern will be addressed later)

## Technologies

* React
* React Router
* RxJS
* Recompose
* GraphQL / Relay
* Express

## Build tools

* NPM
* Browserify
* Babel
* Sass (eww)