image:/images/code.svg
date:2016-05-01
tags:React,FP
synthesis:React est orienté composant et favorise la création de milliers d'entre eux. Cependant, il est important de ne pas se répéter pour garder un code maintenable. Les HOC sont une solution à ce problème.
---
Mais surtout, vous il en exsite déjà une tripotée dans le Grand Internet. Vous pouvez notamment jeter un coup d'oeil du côté de recompose. Cette librairie met à disposition beaucoup de méthodes indispensables à la création d'un composant intelligent.

Parmi les plus utiles il y a :
- mapProps : Transforme les propriétés en entrée par une nouvelle liste de propriétés
- withState : Met à disposition un state
- branch : Affiche un composant ou un autre seulement les propriétés
- compose : Transforme les HOC en paramètre en un seul HOC

La différence notable par rapport aux HoCs que j'ai présenté plus haut est que le BaseComponent est "Curryé". Un petit exemple concret avec notre Fetcher :

```jsx
// Sans Currying
Fetcher(fetch, BaseComponent)
// Avec Currying
Fetcher(fetch)(BaseComponent)
```
Le fetcher va prendre les premiers paramètres et retourner une fonction qui prendra en paramètre le Base Component. Si on transforme notre composant, on a donc :

```jsx
function Fetcher(fetch) {
  return function(BaseComponent) {
    return class... // Le code que l'on avait à l'intérieur du premier fetcher
  }
}
```

Notre Loadable deviendrait :

```jsx
const Loadable = branch(
  function(props) { return props.loading },
  function() { return function(props) { return <div>Chargement en cours...</div> },
  function(BaseComponent) { return BaseComponent }
)
```

```jsx
const Fetcher = compose(
    withState('loading', 'setLoading', true),
    withState('result', 'setResult', {}),
    mapProps()
)
```
