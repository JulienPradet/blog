image:/images/code.svg
date:2016-05-01
tags:React,FP
synthesis:React est orienté composant et favorise la création de milliers d'entre eux. Cependant, il est important de ne pas se répéter pour garder un code maintenable. Les HOC sont une solution à ce problème.
---
Quand on développe une application en React, on est souvent conquis par la simplicité du code. C'est chouette. On a envie d'en mettre partout. Pour moi, cela s'explique surtout par le fait qu'on arrive à prévoir ce qu'elle va faire. Il y a toujours des propriétés en entrée et quelque chose d'affiché en sortie. On est sûr de ne pas avoir d'effet de bord hasardeux. Et ça, c'est le principe d'une fonction pure.

Cependant, il est possible d'en perdre les bénéfices si l'on se sépare trop de la logique fonctionnelle. Par exemple, Si un composant fait tout en même temps, les effets de bords commencent à revenir. On va donc essayer de se rapprocher un peu plus du fonctionnel.

Dans la première partie, nous avons abordé les [Stateless Components](/article/faire-du-react-fonctionnel-partie-1).

Dans cette seconde partie, nous allons introduire la notion de HOC qui évincerons les classes à l'avenir.

Dans les exemples j'évite de mettre un maximum d'astuces liées à ES6. Cependant, usez et abusez des arrow functions. Cela facilite beaucoup la lecture du code.

# Les Higher Order Components (HOC)

C'est une référence aux Higher Order Functions que l'on retrouve dans les langages fonctionnels. Celles-ci sont des fonctions qui prennent en paramètres des fonctions et/ou qui retournent une fonction.

Un HOC est une fonction qui prend en paramètre un composant et qui retourne un composant. Il ajoute alors des fonctionnalités à un composant sans en alourdir l'écriture. La première fois que j'en ai entendu parler, c'est grâce à un article de [Dan Abramov](https://medium.com/@dan_abramov/mixins-are-dead-long-live-higher-order-components-94a0d2f9e750) qui lui même en a entendu parler par [Sebastian Markbåge](https://gist.github.com/sebmarkbage/ef0bf1f338a7182b6775).

Si vous n'êtes pas du monde fonctionnel, ça fait un peu méta et ça a même tendance à terroriser. Mais préférant moi même la pratique à la théorie, on va rapidement revenir sur des exercices

## Quel est l'intérêt ?

> *Faisons plaisir aux architectes et utilisons des mots qui font cools.*

### DRY : Don't Repeat Yourself

En évitant de réécrire plusieurs fois la même logique, vous augmentez la maintenabilité de votre application. En effet, si un bug se glisse dans la logique d'un HOC, vous n'avez qu'à le corriger pour corriger tous les composants qui dépendent de votre HOC.

### SRP : Single Responsability Principle

Les classes de composants se retrouvent souvent avec beaucoup de petites choses à implémenter. Prenons l'exemple d'un menu : il faut récupérer la liste de lien à afficher, mettre un loader le temps que les données s'affichent, mettre en avant le lien actif, mettre en avant le lien que l'utilisateur est entrain de survoler, etc. L'idée derrière les HOCs est d'isoler chaque partie et de les composer pour qu'un composant puisse choisir de quoi il a besoin et que chaque partie puisse être réutilisée.

Ce qui peut être piégeux, c'est de composer des millions de HOCs ce qui donne un composant tout aussi illisible qu'une classe. L'astuce est alors d'isoler une partie de la composition dans une variable que l'on peut nommer et donc expliciter.

## Quid des Mixin ?

Les Mixins sont un mécanisme dont le but est assez similaire à celui des HOCs. Ils rajoutent des fonctionnalités sur des composants. Si vous n'avez jamais entendu parler des Mixins, vous pouvez passer cette partie.

Le concept derrière les Mixins est l'[héritage par concaténation](https://medium.com/javascript-scene/common-misconceptions-about-inheritance-in-javascript-d5d9bab29b0a#.8grf4qua3). Cela veut dire que l'on va prendre un unique composant et qu'on va lui attacher directement des fonctionnalités. Cela veut dire que si deux Mixins ont le même nom de méthode, il va y avoir des conflits ([cf. Gotchas de cet article](http://simblestudios.com/blog/development/react-mixins-by-example.html)).

Je préfère en général l'héritage par composition qui consiste aussi à ajouter des fonctionnalités mais fonctionne en boîte noire : cela veut dire que ce qu'il y a à l'intérieur ne peut pas être altéré. C'est ce qu'offre les HOCs.

De plus, les Mixins n'existent que sur React.createClass. Et je n'aime pas écrire mes composants via la syntaxe en ES5 qui me semble trop verbeuse.

## Exercices : extraire des HOCs

### Loadable Component

Nous allons construire un HOC qui affiche "Chargement en cours..." quand un composant n'est pas prêt à être affiché et qui affiche le composant dès que celui-ci a fini de s'initialiser.

Sans HOC, et si on applique ce que l'on a dit dans la première partie, on aurait quelque chose qui ressemble à :

```jsx
const Profile = (props) => <dl>
  <dt>Prénom</dt>
  <dd>{props.profile.firstname}</dd>
  <dt>Nom</dt>
  <dd>{props.profile.lastname}</dd>
</dl>

const LoadableProfile = (props) => {
  if(props.loading) {
    return <div>Chargement en cours...</div>
  } else {
    return <Profile profile={props.profile}>
  }
}

class SmartProfile extends React.Component {
  // Le constructeur et tout ce qu'il faut pour mettre à jour le state du composant
  // constructor...
  // componentWillUpdate...

  render() {
    return <LoadableProfile loading={this.state.loading} profile={this.state.profile} />
  }
}
```

Cette approche a un inconvénient majeur : à chaque fois qu'un composant devra se charger de manière asynchrone, il faudra réécrire un composant qui ressemblera comme deux gouttes d'eau à `LoadableProfile`, ce qui revient à rejeter le principe du DRY.

*Heureusement, les HOCs, tels Zoro, arrivent à la rescousse !*

Notre premier HOC sera une fonction `Loadable`, que l'on pourra réutiliser pour tout chargement asynchrone dans l'application. Elle aura en paramètre :

* `isLoading` : une fonction qui à partir des propriétés retourne si le composant est entrain de charger ou non.
* `BaseComponent` : le composant à afficher si le chargement est terminé.

```jsx
function Loadable(isLoading, BaseComponent) {
  return class extends React.Component {
    render() {
      if(isLoading(this.props)) {
        return <div>Chargement en cours...</div>
      } else {
        return <BaseComponent {...props} />
      }
    }
  }
}
```

Si l'on revient à notre profil, on peut donc le réécrire de cette façon :

```jsx
const LoadableProfile = Loadable(
  function(props) { return props.loading },
  Profile
)

class SmartProfile extends React.Component {
  // ...
  render() {
    return <LoadableProfile loading={this.state.loading} profile={this.state.profile} />
  }
}
```

## Fetcher Component

Le `Loadable` est pratique, mais on a du mal à y trouver son intérêt étant donné que ce n'est finalement qu'une ternaire.

Nous allons donc faire un HOC qui va devoir gérer un state. L'intérêt de faire de tels HOCs est que pour les composants fils, tout est propriété, et qu'ils n'ont pas besoin de savoir si cela vient d'un singleton, d'un store à la Redux, d'un state...

Pour illustrer ceci, nous allons faire un `Fetcher` : un machin qui fait une requête sur une API et qui affiche un composant quand cette requête s'est bien passée. On utilise ici une class pour faire notre composant puisqu'on a besoin de gérer le cycle de vie du composant via son state.

J'avais dit qu'on n'utiliserait plus de class grâce aux HOCs, mais je n'avais pas précisé que ce serait pour l'écriture des composants finaux (= ceux qui ont la logique métier). Ce n'est pas vraiment une entourloupe étant donné que dans une application, on écrit bien plus souvent ces composants que des HOCs qui seront déjà écrits pour d'autres composants.

```jsx
function Fetcher(
  fetch,
  BaseComponent
) {
  return class extends React.Component {
    constructor(props) {
      super()
      this.state = {loading: true}
      this.setResult = this.setResult.bind(this)
      this.fetch(props)
    }

    componentWillReceiveProps(props) {
      this.setState({ loading: true })
      this.fetch(props)
    }

    fetch(props) {
      fetch(props).then(this.setResult)
    }

    setResult(result) {
      this.setState(Object.assign({ loading: false }, result))
    }

    render() {
      return <BaseComponent {...this.props} {...this.state} />
    }
  }
}

const SmartProfile = Fetcher(
  function(props) {
    return fetch('/users/' + props.id).then(function(profile) {
      return { profile: profile }
    })
  },
  LoadableProfile
)
```

Finalement, on a un HOC qui isole complètement la partie applicative (execution de la requête, gestion du state, etc.) de la partie métier qui est exclusivement dans le composant final (quelle requête effectuer pour récupérer le profil). Si tous les composants sont rédigés de cette façon, la lisibilité du code est grandement accrue.

## Conclusion

Si on résume, pour la gestion d'affichage de profil, les composants finaux sont :

```jsx
const Profile = function(props) {
  return <dl>
    <dt>Prénom :</dt>
    <dd>{props.firstname}</dd>
    <dt>Nom :</dt>
    <dd>{props.lastname}</dd>
  </dl>
}

const LoadableProfile = Loadable(
  function(props) { return props.loading },
  Profile
)

const SmartProfile = Fetcher(
  function(props) {
    return fetch('/users/' + props.id).then(function(profile) {
      return { profile: profile }
    })
  },
  Profile
)

ReactDOM.render(
  <SmartProfile id="julien" />,
  document.getElementById('profile')
)
```

Avec un peu d'habitude et de bons noms de HOC, on comprend du premier coup d'oeil que `LoadableProfile` est un composant qui peut être en chargement et que `SmartProfile` permet juste de récupérer le profil sur une API.

Les points à retenir sont :
- Pour les composants finaux, tout est propriété. Cela veut dire qu'il ne faut pas oublier dans le HOC de passer les données dont a besoin le BaseComponent (i.e. le composant passé en paramètre du HOC).
- Il n'est pas nécessaire de penser en HOC dès le départ. Par contre, il est important de bien repérer les moments où vous vous inspirez de composants que vous avez déjà écrits : si c'est pour copier un fonctionnement, vraisemblablement, vous avez besoin d'isoler cela dans un HOC.
- Il est important de bien dissocier ce que fait chaque HOC pour ne pas faire un HOC spécifique à chaque composant. De plus, en les séparant, vous pouvez toujours les combiner en faisant `HOC1(param1, HOC2(param2, HOC3(param31, param32, BaseComponent)))`. En fait, on pourra même faire mieux. Et ce grâce à [recompose](https://github.com/acdlite/recompose).

Pour bien faire, on parlera de la spécificité de recompose dans la partie 3. Et si ~~vous êtes sages~~ je suis sage, vous aurez même le droit à un exmple.
