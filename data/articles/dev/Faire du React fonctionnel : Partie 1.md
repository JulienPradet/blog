image:/images/code.svg
date:2016-05-03
tags:React,FP
synthesis:React propose une syntaxe à base de classes. Mais tout n'est pas perdu ! Nous avons les stateless components pour rattraper le coup. Le but de cette série est d'expliquer en quoi les stateless components sont bons pour la santé de votre application et comment transformer les classes en fonctions.
---
Quand on développe une application en React, on est souvent conquis par la simplicité du code. C'est chouette. On a envie d'en mettre partout. Pour moi, cela s'explique surtout par le fait qu'on arrive à prévoir ce qu'elle va faire. Il y a toujours des propriétés en entrée et quelque chose d'affiché en sortie. On est sûr de ne pas avoir d'effet de bord hasardeux. Et ça, c'est le principe d'une fonction pure.

Cependant, il est possible d'en perdre les bénéfices si l'on se sépare trop de la logique fonctionnelle. Par exemple, Si un composant fait tout en même temps, les effets de bords commencent à revenir. On va donc essayer de se rapprocher un peu plus du fonctionnel.

Dans cette première partie, on va uniquement s'intéresser aux Stateless Components.

**Prérequis :**

- Avoir déjà entendu parler de React
- Connaître le principe de composant
- Savoir ce qu'est le `state`
- Connaître les arrow functions

# Les Stateless Components

Les Stateless Components sont des composants simplifiés. Petit indice : *state*less. Ils ont l'avantage d'être encore plus prévisibles que les composants habituels puisqu'ils ne peuvent plus avoir de vie propre. Toute modification vient nécessairement des parents. Du coup, on n'a plus besoin d'instancier des classes qui hébergent les données changeantes. On peut directement faire une fonction qui retourne ce qui doit être affiché. En utilisant en plus les syntaxes offertes par ES2015, on peut se retrouver avec des écritures franchement sympas.

```jsx
// Classe
class HelloWorld extends React.Component {
  render() {
    return <h1>Bonjour {this.props.name}</h1>
  }
}

// Fonction
function HelloWorld(props) {
    return <h1>Bonjour {props.name}</h1>
}

// Arrow function et destructuring
const HelloWorld = ({name}) => <h1>
    Bonjour {name}
</h1>
```

## Quel est l'intérêt ?

A part le fait d'économiser des lignes de code, c'est assez difficile de se faire une idée sur l'exemple ci dessus. Cependant, avoir une approche Stateless par défaut a plusieurs avantages.

### La séparation des composants *Smart*/*Dumb*

Un Smart component est un composant qui contient de la logique métier. Un Dumb component est uniquement responsable de la vue.

En utilisant en priorité les Stateless, la gestion d'état de l'application va naturellement remonter dans des composants *Smart* plus globaux. En effet, s'imposer de commencer par écrire des Stateless quitte à les transformer en classe plus tard, nous oblige à réfléchir si cela vaut vraiment le coup de stocker dans l'élément actuel plutôt que dans le père. Au contraire, quand on utilise les classes, on veut faire fonctionner le bousin, quitte à le modifier plus tard, et on se retrouve avec des responsabilités mal partagées.

L'inverse peut aussi se passer : certaines opérations ont plus de sens dans un sous-composant afin de simplifier l'API qu'expose le composant parent. C'est généralement un comportement applicatif (bouton qui s'active, formulaire qui se met à jour, etc.) qui est de la responsabilité d'un sous-composant contrairement à la logique métier qui doit être centralisée.

### La prolifération de composants unitaires

Les composants résultants sont souvent plus unitaires. En effet, une nouvelle fonction ne coutant rien à écrire, on va tendre vers plus de petits composants. Un bon exercice pour mettre cela en pratique est d'interdire l'utilisation du mot clé `if` et de le remplacer par des expressions ternaires (`test ? ifTrue : ifFalse`). Si la ternaire devient illisible c'est qu'il est nécessaire d'extraire un sous-composant.

En ajoutant à cela un bon nommage, la lisibilité globale s'améliore puisqu'on remplace des successions de `div`s, `span`s et autres joyeusetés par des objets identifiables au premier coup d'oeil. Par exemple :

```jsx
// Ceci :
<div className="menu">
  <div className="menu__left">
    <ul>
      <li>Item 1</li>
      <li>Item 2</li>
      <li>Item 3</li>
    </ul>
  </div>
  <div className="menu__right">
    <form>
      <label>Search</label>
      <input />
      <button>Search</button>
    </form>
  </div>
</div>

// Devient :
<MenuContainer>
  <Navigation />
  <Search />
</MenuContainer>
```

### Une testabilité accrue
Les composants sont plus facilement testables étant donné qu'ils n'ont que des propriétés en entrée, et qu'ils affichent *vraiment* toujours la même chose, on peut tester leur affichage séparement de la logique de l'application.

Cela peut être des tests fonctionnels ou des tests visuels. Automatiques ou non. Je n'ai pas encore assez d'expérience sur le sujet mais le milieu est en effervescence. On peut notamment regarder du côté de Enzyme, Ava, Storybook, ...

## Exercice : Refactorer une classe

> De belles palabres ! Mais nous voulons de l'action nous ! Bouuuuh !

Dans ce cas, nous allons de manière successive refactorer cette classe :

```
class LoginForm extends React.Component {
  constructor() {
    super()
    this.state = {
      login: '',
      password: ''
    }
    this.handleLogin = this.handleLogin.bind(this)
  }

  handleLogin() {
    this.setState({
      loading: true
    })

    fetch(
      '/login',
      { method: 'POST', ... }
    )
      .then((token) => {
        this.setState({ token: token })
        this.props.login(token)
      })
      .catch((error) => console.debug(error))
      .finally(() => this.setState({
        loading: false
      })
  }

  onChange(name) {
    return function(event) {
      this.setState({
        [name]: event.currentTarget.value
      })
    }
  }

  renderLoginFormInputs() {
    return <form onSubmit={this.handleLogin}>
      <div>
        <label>Login</label>
        <input type="text" name="login" onChange={this.onChange('login')} />
      </div>
      <div>
        <label>Password</label>
        <input type="password" name="password" onChange={this.onChange('password')} />
      </div>
      <div>
        <button type="submit">Login</button>
      </div>
    </form>
  }

  renderAccountActions() {
    return <div>
      <a href="/forgottenpassword">Mot de passe oublié ?</a>
      <a href="/register">S'inscrire</a>
    </div>
  }

  render() {
    if(this.state.loading) {
      return <div>Connexion en cours...</div>
    } else if(this.state.token) {
      return <div>Bienvenue {this.state.login}</div>
    } else {
      return <div>
        <h1>Formulaire de connexion</h1>
        {this.renderLoginFormInputs()}
        {this.renderAccountActions()}
      </div>
    }
  }
}
```

Ce n'est pas la classe la plus propre du monde, mais ce n'est pas non plus une classe qui a été salie outre mesure. Par exemple, une partie du JSX a été isolé dans des render différents. Quelqu'un qui a un peu l'habitude de lire du code React arrivera assez facilement à comprendre ce qu'il se passe. Par contre, quelqu'un qui débute ou qui n'a pas l'habitude de lire du code aura plus de mal. C'est l'indice qu'il y a quelque chose à faire.

Pour l'instant nous allons nous intéresser uniquement à la partie `render` étant donné que nous apprenons à utiliser les Stateless. Nous effectuerons le reste de la refactorisation dans les parties 2 et 3 du tutoriel.

### Etape 1 : Séparation Smart/Dumb

```jsx
class LoginForm extends React.Component {
  // Toutes les fonctions qui ne commencent pas comme `render`

  render() {
    return <DumbLoginForm
      loading={this.state.loading}
      login={this.state.login}
      onChange={this.onChange}
      handleLogin={this.handleLogin}
    />
  }
}

const DumbLoginForm = (props) => {
  if(props.loading) {
    return <div>Connexion en cours...</div>
  } else if(props.token) {
    return <div>Bienvenue {props.login}</div>
  } else {
    return <div>
      <h1>Formulaire de connexion</h1>
      <LoginFormInputs onChange={props.onChange} handleLogin={props.handleLogin} />
      <AccountActions />
    </div>
  }
}

const LoginFormInputs = (props) => <form onSubmit={props.handleLogin}>
 <div>
   <label>Login</label>
   <input type="text" name="login" onChange={props.onChange('login')} />
 </div>
 <div>
   <label>Password</label>
   <input type="password" name="password" onChange={props.onChange('password')} />
 </div>
 <div>
   <button type="submit">Login</button>
 </div>
</form>

const AccountActions = (props) => <div>
  <a href="/forgottenpassword">Mot de passe oublié ?</a>
  <a href="/register">S'inscrire</a>
</div>
```

Récapitulatif de l'étape :

1. Isolation du render dans un DumbComponent. Il est maintenant possible de tester chaque combinaison de propriété sans avoir à trafiquer notre logique métier.
2. Extraction des `renderMachinTruc` dans des composants séparés. En effet si on regarde de plus près leur implémentation, ce ne sont que des fonctions qui prennent en paramètre des propriétés cachées (à partir du this). En les mettant dans de vraies propriétés, on explicite leur fonctionnement et on allège le composant parent.

### Etape 2 : Isolation des composants et dédupplication du code

```jsx
const DumbLoginForm = (props) => {
  if(props.loading) {
    return <Loading />
  } else if(props.token) {
    return <WelcomeUser user={props.login} />
  } else {
    return <Login onChange={props.onChange} handleLogin={props.handleLogin} />
  }
}

const Login = (props) => <div>
  <h1>Formulaire de connexion</h1>
  <LoginFormInputs onChange={props.onChange} handleLogin={props.handleLogin} />
  <AccountActions />
</div>

const LoginFormInputs = (props) => <form onSubmit={props.handleLogin}>
  <FormInput label="Login" name="login" type="text" onChange={props.onChange('login')} />
  <FormInput label="Password" name="password" type="password" onChange={props.onChange('password')} />
  <div>
    <button type="submit">Login</button>
  </div>
</form>

const FormInput = (props) => <div>
  <label>{props.label}</label>
  <input type={props.type} name={props.name} onChange={props.onChange} />
</div>

const AccountActions = (props) => <div>
  <a href="/forgottenpassword">Mot de passe oublié ?</a>
  <a href="/register">S'inscrire</a>
</div>

const Loading = (props) => <div>Connexion en cours...</div>

const WelcomeUser = (props) => <div>Bienvenue {props.user}</div>
```

Récapitulatif de l'étape :

1. Isolation de chaque bloc sémantique dans le `DumbLoginForm`. En effet, même si aujourd'hui les composants sont simples, le fait de les séparer permet de réfléchir au rôle de chacun de ses composants (quel est le but de ce cas métier ?) tout en permettant d'évoluer plus sereinement à l'avenir.
2. Déduplication du code dans le `LoginFormInputs`. Le login et le password sont tout à fait identiques à quelques paramètres près. Si on veut ajouter une classe CSS à l'un, on voudra vraisemblablement l'ajouter à l'autre. Le fait de n'avoir qu'un seul composant permet de ne pas rater ses copier/coller.

### Etape 3 : Réévaluation des responsabilités

Cette fois, je pense qu'une petite introduction est nécessaire avant de montrer le code. La plupart du temps on veut remonter un maximum de logique dans un composant *racine*. C'est dans l'ensemble une bonne pratique parce que cela permet de ne pas éparpiller son code. Cependant, il faut bien faire la distinction entre la logique métier et la logique applicative. La logique métier n'est même pas censée savoir comment fonctionne l'application. Elle a uniquement accès à une API simplifiée pour bien séparer les responsabilités.

Ce genre de problèmes se sent quand on passe beaucoup de propriétés de composants en composants.

Dans notre exemple, on constate que c'est le cas au niveau du formulaire de login. Le `SmartFormLogin` *sait* qu'il y a des valeurs qui changent au cours du temps qui sont sauvegardées dans son state et qui sont réutilisées au moment de la soumission du formulaire. Pourtant, il pourrait ne pas le savoir. Il a uniquement besoin de savoir qu'un utilisateur a tenté de se connecter avec un login et un password. Ainsi, si l'on, change la facon de récupérer le login/password, cela n'aura pas d'impact sur le `SmartFormLogin`. Si on traduit cela en terme de code, cela donne :

```jsx
class LoginForm extends React.Component {
  constructor() {
    super()
    this.state = {
      user: ''
    }
    this.handleLogin = this.handleLogin.bind(this)
  }

  handleLogin(login, password) {
    this.setState({
      loading: true
    })

    fetch(
      '/login',
      { method: 'POST', ... }
    )
      .then((token) => {
        this.setState({ login: login })
      })
      .catch((error) => console.debug(error))
      .finally(() => this.setState({
        loading: false
      })
  }

  render() {
    return <DumbLoginForm
      loading={this.state.loading}
      login={this.state.login}
      handleLogin={this.handleLogin}
    />
  }
}

class LoginFormInputs extends React.Component {
  constructor() {
    super()
    this.state = {
      login: '',
      password: ''
    }
  }

  onChange(name) {
    return function(event) {
      this.setState({ [name]: event.currentTarget.value })
    }
  }

  render() {
    return <form onSubmit={this.props.handleLogin}>
      <FormInput label="Login" name="login" type="text" onChange={this.state.onChange('login')} />
      <FormInput label="Password" name="password" type="password" onChange={this.state.onChange('password')} />
      <div>
        <button type="submit">Login</button>
      </div>
    </form>
  }
}
```

### Récapitulatif global

Nous avons maintenant plein de petits composants. Dans l'ensemble le code est plus long que ce que nous avions avant. Mais pour moi ce n'est pas un problème. En effet, la longueur est importante si elle empêche de comprendre le fonctionnement de l'application. Ici, on a uniquement besoin de lire et comprendre le SmartLoginForm et le DumbLoginForm. Le reste n'est qu'un détail d'implémentation. Et ceci reste vrai pour chacun des sous composants. De plus, les composants tels que `FromInput` ou `Loading` seront vraisemblablement réutilisés dans le reste de l'application.

La prochaine étape de refactorisation sera de réussir à extraire le fonctionnement des classes dans des HOCs (pleins de gros mots à venir dans les parties 2 et 3 du tuto).

## Conclusion

Pour l'instant nous allons donc nous arrêter là en attendant la suite de la série.

Les points à retenir sont :

- Il est difficile de se forcer à extraire les composants. Pour cette raison, ne jamais écrire de classe à moins d'y être contraint. Et quand cela arrive, transformer une fonction existante en classe, plutôt que de se dire dès le début que c'est censé être une classe.

- Revenir sur des composants que l'on a écrit un ou deux jours après les avoir écrit pour prendre du recul et mieux discerner les patterns. Essayer de faire régulièrement des revues de code. Les autres ont naturellement le recul qu'il est difficile de prendre sur son propre code.

- Dans le cadre d'une refactorisation ne pas chercher à faire quelque chose de propre dès le début. En séparant les choses petit à petit, le modèle mental de l'application sera plus clair et les solutions viendront plus naturellement. Cependant, c'est une démarche particulière qu'il est difficile à aborder au début. On s'y perd souvent et il faut se prendre les pieds dans le tapis à plusieurs reprises avant que cela soit naturel.

- Pour une création from scratch, essayer d'être rigoureux dans le processus. Une méthode qui fonctionne pour moi :

  - Toujours commencer par écrire les composants racines et ne pas utiliser de balise HTML
  - Créer les composants qui ont émergé en procédant de la même manière jusqu'à ce que le HTML soit indispensable.
  - Ajouter l'intéractivité en mettant en place du state, etc. En repoussant cette étape au dernier moment, on aplatit tous les besoins avant de se lancer dans la logique métier.

Ces méthodes sont le fruit de lectures, d'expérimentations et de partage. Qu'en pensez-vous ? Vous semblent-t-elles judicieuses ou préférez rester sur les bonnes vieilles classes ?
