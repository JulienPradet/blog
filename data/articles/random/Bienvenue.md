image:https://s3.amazonaws.com/uifaces/faces/twitter/hai_ninh_nguyen/128.jpg
date:2016-04-30
tags:A props
synthesis:Je suis tout fou de vous présenter ce nouveau petit blog. Ce n'est pas grandiose mais je sens que ça va être un très chouette terrain de jeu et d'échange.
---
# Hi there !

Moi, Julien Pradet, n-ième du nom vous souhaite la bienvenue !

## Pourquoi un blog ?

J'ai créé ce blog parce que c'était l'occasion pour moi d'avoir une plateforme sans prétention sur laquelle je peux tester de nouvelles choses et partager mes expériences. Du coup c'est une solution overkill-pas-finie qui vivra et verra sûrement beaucoup de changements au cours de sa vie.

## Et ça parlera de quoi ?

Etant multiple, je ne me restreindrai cependant pas à mes seules occupations professionnelles (développement web). Je compte aussi partager des créations imagées - parce que je ne suis pas sûr qu'on puisse appeler ça de l'illustration - et tout ce qui peut bien me passer par la tête. Ce sera un joyeux bordel.

## Et niveau rythme de publication ?

Pas spécialement. Mon but est de publier au moins une fois par mois. Mais il y a des chances que je publie des choses sur le site d'Occitech plutôt qu'ici. Donc à voir ;D

Cela dit, si vous avez des idées d'articles, des questions ou quoique ce soit qui vous vient à l'esprit, vous pouvez venir me trouver sur mon compte Twitter @JulienPradet ou sur le dépôt git du blog en créant une issue.

Au plaisir !