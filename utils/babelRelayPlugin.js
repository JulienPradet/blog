var getbabelRelayPlugin = require('babel-relay-plugin');
var schema = require('../dist/server/data/schema.json');

module.exports = getbabelRelayPlugin(schema.data);
