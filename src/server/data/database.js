import path from 'path'
import Rx from 'rx'
import {readfile, readdir, stat} from '../util/fs'


const articlesDir = path.join(__dirname, '../../../data/articles')

export const articles$ = readdir(articlesDir)
  .flatMap((categories) => categories.map(readdir))
  .flatMap((identity) => identity)
  .flatMap((files) => files.map(readfile))
  .flatMap((identity) => identity)
  .flatMap((file) => stat(file.path).withLatestFrom(
    Rx.Observable.just(file),
    (stats, file) => Object.assign({}, file, stats)
  ))
  .map((file) => {
    const category = file.path.substring(articlesDir.length + 1).match(/^\w+/)[0]

    const regexp = /(([^:]+):([^\n]+))\n*/g
    const data = file.file

    const metaIndex = data.indexOf('---')
    const metaString = data.substring(0, metaIndex)
    const content = data.substring(metaIndex + 3);

    let match, key, value
    const metaData = {}

    while ((match = regexp.exec(metaString)) !== null) {
      key = match[2]
      value = match[3]
      metaData[key] = value
    }

    const synthesis = content.length > 200 ? (content.substring(0, 200) + '...') : content

    return Object.assign({}, file, {
      category: category,
      content: content,
      synthesis: synthesis
    }, metaData)
  })
