import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull
} from 'graphql'

import {
  fromGlobalId,
  globalIdField,
  nodeDefinitions,
  connectionDefinitions,
  connectionFromArray,
  connectionArgs
} from 'graphql-relay'

import {
  Blog,
  getBlog,
  Article,
  getArticles,
  getArticle,
  getNextArticle,
  getPreviousArticle
} from './articles'

const {nodeInterface, nodeField} = nodeDefinitions(
  (globalId) => {
    const {type, id} = fromGlobalId(globalId)
    if (type === 'Article') {
      return getArticle(id)
    } else if (type === 'Blog') {
      return getBlog()
    } else {
      return null
    }
  },
  (obj) => {
    if (obj instanceof Article) {
      return articleType
    } else if (obj instanceof Blog) {
      return blogType
    } else {
      return null
    }
  }
)

const articleType = new GraphQLObjectType({
  name: 'Article',
  fields: () => ({
    id: globalIdField('Article'),
    slug: {
      type: GraphQLString,
      description: 'Article\'s unique identifier',
      label: 'Slug'
    },
    title: {
      type: GraphQLString,
      label: 'Title'
    },
    content: {
      type: GraphQLString,
      label: 'Content',
      description: 'Content of the article in html'
    },
    synthesis: {
      type: GraphQLString,
      label: 'Synthesis',
      description: 'Synthesis of the article in html'
    },
    image: {
      type: GraphQLString,
      label: 'Image',
      description: 'URL of the image that is featured in the article'
    },
    date: {
      type: GraphQLString,
      label: 'Date',
      description: 'Publication date'
    },
    type: {
      type: GraphQLString,
      label: 'Type',
      description: 'Category of the article'
    },
    tags: {
      type: new GraphQLList(GraphQLString),
      label: 'Tags',
      description: 'List of tags assigned to the article'
    },
    next: {
      type: articleType,
      label: 'Next article',
      description: 'Article that should be read next',
      resolve: (article) => getNextArticle(article.slug)
    },
    previous: {
      type: articleType,
      label: 'Previous article',
      description: 'Article that should be read previously',
      resolve: (article) => getPreviousArticle(article.slug)
    }
  }),
  interfaces: [nodeInterface]
})
const {connectionType: typeConnection} = connectionDefinitions({nodeType: articleType})

const RootQuery = new GraphQLObjectType({
    name: 'RootQuery',
    description: 'Root query',
    fields: () => ({
      // HACK to make queries at root level
      rootHack: {
          type: new GraphQLNonNull(RootQuery),
          resolve: () => ({})
      },
      articleList: {
        name: 'ArticleList',
        type: typeConnection,
        args: Object.assign({filter: {
          type: GraphQLString,
          label: 'Filter',
          description: 'Filter for articles'
        }}, connectionArgs),
        resolve: (_, args) => connectionFromArray(
          getArticles(args.filter),
          args
        )
      },
      article: {
        name: 'Article',
        type: articleType,
        args: {slug: {
          type: GraphQLString,
          label: 'Slug',
          description: 'Article\'s unique slug'
        }},
        resolve: (_, {slug}) => getArticle(slug)
      }
    })
})

export const Schema = new GraphQLSchema({
    query: RootQuery
})
