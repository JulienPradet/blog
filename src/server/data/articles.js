import Prism from 'prismjs'
import 'prismjs/components/prism-jsx'
import markdown from 'markdown-it'
import slug from 'slug'
import faker from 'faker'

import {articles$} from './database'

const md = markdown({
  html:         false,
  xhtmlOut:     false,
  breaks:       false,
  linkify:      false,
  typographer:  false,
  quotes: '“”‘’',
  highlight: function (code, lang = 'jsx') {
    if(!Prism.languages.hasOwnProperty(lang)) {
      lang = 'jsx'
    }
    return '<pre class="language-' + lang + '"><code>' + Prism.highlight(code, Prism.languages[lang]) + '</code></pre>';
  }
})

export class Article {
  constructor({ slug, title, date, content, synthesis, image, type, tags }) {
    Object.assign(this, {
      id: slug,
      slug: slug + "",
      title,
      date: date,
      content: md.render(content),
      synthesis: md.render(synthesis),
      image,
      type: type || 'random',
      tags: tags ? tags.split(',') : []
    })
  }
}

articles$
  .map((file) => new Article({
    slug: slug(file.name).toLowerCase(),
    date: file.date,
    image: file.image || faker.image.avatar(),
    title: file.name,
    content: file.content,
    synthesis: file.synthesis,
    type: file.category,
    tags: file.tags
  }))
  .reduce(
    (acc, file) => [...acc, file],
    []
  )
  .map((articles) => articles.sort(function(a, b) {
    return Date.parse(b.date) - Date.parse(a.date)
  }))
  .subscribe(updateArticles)

let articlesData = []
function updateArticles(articles) {
  articlesData = articles
}

export const getArticles = (filter) => {
  if(!filter || filter.length === 0) {
    return articlesData
  }
  return articlesData.filter((article) => filter.indexOf(article.type) !== -1)
}

export const getArticle = (slug) => articlesData.find((article) => article.slug === slug)

export const getNextArticle = (previousSlug) => {
  const previousIndex = articlesData.findIndex((article) => article.slug === previousSlug)
  if(previousIndex >= articlesData.length - 1) {
    return null
  } else {
    return articlesData[previousIndex + 1]
  }
}

export const getPreviousArticle = (nextSlug) => {
  const nextIndex = articlesData.findIndex((article) => article.slug === nextSlug)
  if(nextIndex <= 0) {
    return null
  } else {
    return articlesData[nextIndex - 1]
  }
}
