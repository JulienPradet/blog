import path from 'path'
import express, { Router } from 'express'
import graphQLHTTP from 'express-graphql'
import { Schema } from './data/schema'

const app = express()

app.use('/', express.static(path.join(__dirname, '../client/public')))

app.use('/graphql', graphQLHTTP({
  graphiql: true,
  pretty: true,
  schema: Schema
}))

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/public/index.html'))
})

const webServer = app.listen(3000, function () {
  var host = webServer.address().address
  var port = webServer.address().port

  console.log('Access Blog at http://%s:%s', host, port)
})
