import path from 'path'
import fs from 'fs'
import Rx from 'rx'

export function getFilenameMetaData(filePath) {
  const extension = path.extname(filePath);
  const filename = path.basename(filePath, extension);

  return {
    extension: extension,
    name: filename,
    location: path.dirname(filePath),
    path: filePath
  }
}

export function readdir(dirPath) {
  return Rx.Observable.create((observer) => {
    fs.readdir(dirPath, (e, files) => {
      if(e) return observer.onError(e)

      files = files.map((file) => path.join(dirPath, file))
      observer.onNext(files)
      observer.onCompleted()
    })
  })
}

export function readfile(filePath) {
  return Rx.Observable.create((observer) => {
    fs.readFile(filePath, (e, file) => {
      if(e) return observer.onError(e)

      const data = Object.assign(getFilenameMetaData(filePath), { file: file.toString() })
      observer.onNext(data)
      observer.onCompleted()
    })
  })
}

export function stat(filePath) {
  return Rx.Observable.create((observer) => {
    fs.stat(filePath, (e, stats) => {
      if(e) return observer.onError(e)

      const data = Object.assign({
        date: stats.birthtime,
        editedAt: stats.mtime
      })

      observer.onNext(data)
      observer.onCompleted()
    })
  })
}