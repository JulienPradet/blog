import React from 'react'
import Relay from 'react-relay'
import { Link } from 'react-router'
import { compose, withState, mapProps, withHandlers } from 'recompose'

import ArticlePreview from './ArticlePreview'
import * as Layout from './component/organism/Layout'
import * as Button from './component/atom/Button'

const filters = {
  'dev': 'Développement web',
  'illu': 'Illustration',
  'random': 'Random'
}

const pageSize = 10

const ArticleListHead = (props) => <Layout.Container>
  <Layout.Column size={1}>
    { Object.keys(filters).map((filter) => {
      const active = props.activeFilters.indexOf(filter) !== -1
      const link = active ? "/" : "/" + filter
      return <Button.Simple
          key={filter}
          size="big"
          type="filter"
          active={active}
          to={link}>
        {filters[filter]}
      </Button.Simple>
    })}
  </Layout.Column>
</Layout.Container>

const ArticleListContent = (props) => <Layout.Container>
  { props.articleList.map((article, index) => <Layout.Column size={1} key={article.slug}>
    <ArticlePreview article={article} />
  </Layout.Column> )}
</Layout.Container>

const ArticleListFooter = (props) => <Layout.Container>
  { props.hasNext ? <Button.Simple to={props.loadMoreRoute}>Afficher plus d'articles</Button.Simple> : null }
</Layout.Container>

const StaticArticleList = (props) => {
  return <div>
    <ArticleListHead activeFilters={props.filters} />
    <ArticleListContent articleList={props.articleList} />
    <ArticleListFooter hasNext={props.hasNext} hasPrevious={props.hasPrevious} loadMoreRoute={props.loadMoreRoute} />
  </div>
}

const ArticleList = compose(
  mapProps((props) => Object.assign({}, props, {
    articleList: props.articleListQuery.articleList.edges.map(({node}) => node),
    hasNext: props.articleListQuery.articleList.pageInfo.hasNextPage,
    hasPrevious: props.articleListQuery.articleList.pageInfo.hasPrevious,
    params: Object.assign({}, props.params, {
      page: parseInt(props.params.page) || 1
    })
  })),
  mapProps((props) => Object.assign({}, props, {
    filters: props.params.filters ? props.params.filters.split(',') : '',
    loadMoreRoute: '/' + props.params.filters + '/' + (props.params.page + 1)
  }))
)(StaticArticleList)

const ArticleListContainer = Relay.createContainer(ArticleList, {
  initialVariables: {
    filters: '',
    pageSize: pageSize,
    offset: 0
  },
  fragments: {
    articleListQuery: () => Relay.QL`
      fragment on RootQuery  {
        articleList (first: $pageSize, filter: $filters) {
          edges {
            node {
              slug
              ${ArticlePreview.getFragment('article')}
            }
          }
          pageInfo {
            hasNextPage
            hasPreviousPage
          }
        }
      }
    `
  }
})

ArticleListContainer.queries = {articleListQuery: () => Relay.QL`query { rootHack }`}
ArticleListContainer.routeName = 'articleList'
ArticleListContainer.paramHandler = (params) => ({
  filters: params.filters || '',
  pageSize: (params.page || 1) * pageSize
})

export default ArticleListContainer
