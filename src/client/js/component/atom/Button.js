import React from 'react'
import { Link } from 'react-router'
import classNames from 'classnames'

export const Simple = ({size, type, active, onClick, children, to}) => {
  const Container = onClick ? 'button' : to ? Link : 'span'
  return <Container
    className={classNames('btn', 'btn--'+(type || 'link'), {'btn--active': active, 'btn--big': size === 'big'})}
    to={to}
    onClick={(event) => {
      event.currentTarget.blur()
      onClick ? onClick() : null
    }}>
    {children}
  </Container>
}
