import React from 'react'
import classNames from 'classnames'
import {compose, withState, mapProps, withHandlers, withContext, getContext, onlyUpdateForKeys} from 'recompose'

const StaticContainer = (props) => <div className={classNames('media', {'media--active': props.expanded, 'media--big': props.type === 'big'})} onMouseEnter={props.onMouseEnter} onMouseLeave={props.onMouseLeave}>
  {props.children}
</div>

export const Container = compose(
  withState('expanded', 'setExpanded', false),
  withHandlers({
    onMouseEnter: (props) => (event) => {
      props.setExpanded(true)
    },
    onMouseLeave: (props) => (event) => {
      props.setExpanded(false)
    }
  }),
  mapProps((props) => Object.assign({}, props, {
    expanded: props.expanded || props.type === 'big'
  })),
  onlyUpdateForKeys(['children', 'expanded']),
  withContext(
    { expanded: React.PropTypes.bool.isRequired },
    (props) => ({ expanded: props.expanded })
  )
)(StaticContainer)

export const Picture = (props) => <div className="media__picture">
  {props.children}
</div>

const StaticContent = (props) => <div className={classNames("media__content", {"media__content--active": props.expanded})}>
  {props.children}
</div>

export const Content = getContext(
  { expanded: React.PropTypes.bool.isRequired }
)(StaticContent)

export const Title = (props) => <div className="media__content__title">
  {props.children}
</div>

export const Meta = (props) => <div className="media__content__meta">
  {props.children}
</div>

const StaticDescription = (props) => <div className={classNames("media__content__description", {'media__content__description--active': props.expanded})}>
  {props.children}
</div>

export const Description = getContext(
  { expanded: React.PropTypes.bool.isRequired }
)(StaticDescription)
