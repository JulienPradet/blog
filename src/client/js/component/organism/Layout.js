import React from 'react'

export const Container = (props) => <div className="layout">
  {props.children}
</div>

export const Column = (props) => <div className={"layout__col--" + props.size}>
  {props.children}
</div>
