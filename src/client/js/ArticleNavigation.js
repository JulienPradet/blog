import React from 'react'
import Relay from 'react-relay'
import { Link } from 'react-router'
import classNames from 'classnames'

import * as Layout from './component/organism/Layout'

const ArticleLink = (props) => <Link to={"/article/" + props.article.slug} className={classNames("navigation", {[`navigation--${props.type}`]: props.type})}>
  {props.article.title}
</Link>

const ArticleLinkContainer = Relay.createContainer(ArticleLink, {
  fragments: {
    article: () => Relay.QL`
      fragment on Article {
        slug
        title
      }
    `
  }
})

const ArticleNavigation = (props) => <Layout.Container>
  {props.article.previous ? <ArticleLinkContainer type="previous" article={props.article.previous} /> : null}
  <span className="fade">|</span>
  {props.article.next ? <ArticleLinkContainer type="next" article={props.article.next} /> : null}
</Layout.Container>

const ArticleNavigationContainer = Relay.createContainer(ArticleNavigation, {
  fragments: {
    article: () => Relay.QL`
      fragment on Article {
        previous {
          ${ArticleLinkContainer.getFragment('article')}
        }
        next {
          ${ArticleLinkContainer.getFragment('article')}
        }
      }
    `
  }
})

export default ArticleNavigationContainer
