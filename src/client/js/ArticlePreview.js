import React from 'react'
import Relay from 'react-relay'
import { Link } from 'react-router'

import * as Media from './component/molecule/Media'
import {Avatar, Title, PublishedAt, Tags, Content} from './Article'

const ArticlePreview = (props) => <Link to={"/article/" + props.article.slug}>
  <Media.Container>
    <Media.Picture>
      <Avatar src={props.article.image} />
    </Media.Picture>
    <Media.Content>
      <Media.Title>
        <Title title={props.article.title} />
      </Media.Title>
      <Media.Meta>
        <PublishedAt date={props.article.date} />
        <Tags tags={props.article.tags} />
      </Media.Meta>
      <Media.Description>
        <Content content={props.article.synthesis} />
      </Media.Description>
    </Media.Content>
  </Media.Container>
</Link>

export default Relay.createContainer(ArticlePreview, {
  fragments: {
    article: () => Relay.QL`
      fragment on Article {
        slug
        title
        synthesis
        date
        image
        tags
      }
    `
  }
})
