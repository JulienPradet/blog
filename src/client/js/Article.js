import React from 'react'
import Relay from 'react-relay'
import { Link } from 'react-router'
import path from 'path'

import * as Media from './component/molecule/Media'
import * as Layout from './component/organism/Layout'
import ArticleNavigation from './ArticleNavigation'

export const Avatar = (props) => {
  return <img width="128" height="128" src={props.src} alt="" />
}

export const Title = (props) => <span>{props.title}</span>

const months = [
  'janvier',
  'février',
  'mars',
  'avril',
  'mai',
  'juin',
  'juillet',
  'août',
  'septembre',
  'octobre',
  'novembre',
  'décembre'
]

export const PublishedAt = (props) => {
  const date = new Date(props.date)
  const day = date.getDate()
  const monthIndex = date.getMonth()
  const month = months[monthIndex]
  const year = date.getFullYear()

  return <div>le {day} {month} {year}</div>
}

export const Tags = (props) => {
  return <div>Thèmes : {props.tags.join(', ')}</div>
}

export const Content = (props) => <div dangerouslySetInnerHTML={{__html: props.content}}></div>

const Article = (props) => <Layout.Container>
  <Media.Container type="big">
    <Media.Picture>
      <Avatar src={props.article.image} />
    </Media.Picture>
    <Media.Content>
      <Media.Title>
        <Link to={"/article/" + props.article.slug}>
          <Title title={props.article.title} />
        </Link>
      </Media.Title>
      <Media.Meta>
        <PublishedAt date={props.article.date} />
        <Tags tags={props.article.tags} />
      </Media.Meta>
      <Media.Description>
        <Content content={props.article.content} />
      </Media.Description>
    </Media.Content>
  </Media.Container>

  <ArticleNavigation article={props.article} />
</Layout.Container>

const ArticleContainer = Relay.createContainer(Article, {
  fragments: {
    article: () => Relay.QL`
      fragment on Article {
        slug
        title
        content
        date
        image
        ${ArticleNavigation.getFragment('article')},
        tags
      }
    `
  }
})


ArticleContainer.queries = {article: () => Relay.QL`query { article (slug: $slug) }`}
ArticleContainer.routeName = 'article'

export default ArticleContainer
