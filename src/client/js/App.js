import React from 'react'
import Relay from 'react-relay'
import { Router, IndexRoute, Route, Link, browserHistory } from 'react-router'

import * as Layout from './component/organism/Layout'
import ArticleList from './ArticleList'
import Article from './Article'

function createRelayContainer (Component, props) {
  if (Relay.isContainer(Component)) {
    const {routeName, queries, paramHandler} = Component
    if (!routeName || !queries) {
      throw new TypeError('A container that is used as a Route\'s Component should always have the statics `routeName` and `queries` in order to define the Relay.RootContainer.')
    }
    const params = paramHandler ? paramHandler(props.params) : props.params || {}

    return (
      <Relay.RootContainer
        Component={Component}
        renderFetched={(data) => <Component {...props} {...data} />}
        route={{name: routeName, params, queries}}
      />
    )
  } else {
    return <Component {...props} />
  }
}

const App = (props) => <div>
  <Layout.Container>
    <h1><Link to="/">Kékidi ?</Link></h1>
  </Layout.Container>
  {props.children}
</div>

export default (props) => <Router history={browserHistory} createElement={createRelayContainer}>
  <Route path="/" component={App}>
    <IndexRoute component={ArticleList} />
    <Route path="/article/:slug" component={Article} />
    <Route path="/(:filters)(/:page)" component={ArticleList} />
  </Route>
</Router>
